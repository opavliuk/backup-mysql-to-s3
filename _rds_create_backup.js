/**
 * _rds_create_backup.js
 *
 * This script gives the automated process of creating backup
 * MySQL DB from the DB Instance in AWS RDS and storing it in
 * AWS S3 Bucket.
 *
 * Before using this script, sure that you have full-access
 * permissions to AWS RDS and write permissions to AWS S3 bucket.
 *
 * @version - 1.0.0
 * @author  - Oleksii Pavliuk pavliuk.aleksey@gmail.com
 * @date    - 2020-08-10
 *
 */

// Turn ON strict mode
"use strict";

// A common global configurations:
global.Config = require(__dirname + '/config/_config');

// An app configurations:
global.App = {
    base_dir: __dirname,
    config: require(__dirname + '/config/rds_create_backup.json'),

    // Load NPM Modules:
    lib: require('./_library/_autoload'),
    spawn: require('child_process').spawn,
    zlib: require('zlib')
};

/**
 * Create MySQL Database Backup.
 *
 * Creates a backup of MySQL DB via mysqldump and,
 * stores it AWS S3 bucket.
 */
// Global Variables:
let _db_host = App.config.db_host,
    _db_user = App.config.db_user,
    _db_pass = App.config.db_pass,
    _db_name = App.config.db_name,

    _mysql_connect_params = {
        host: _db_host,
        user: _db_user,
        password: _db_pass,
        database: _db_name
    },

    /**
     * Flags for mysqldump command:
     *      * --column-statistics: It disables default using column statistics.
     *                      For avoid error: Unknown table 'COLUMN_STATISTICS' in information_schema (1109).
     *                      Probably this error is due to versions difference.
     *      * --single-transaction: https://dev.mysql.com/doc/refman/5.6/en/mysqldump.html#option_mysqldump_single-transaction
     *      * --quick: https://dev.mysql.com/doc/refman/5.6/en/mysqldump.html#option_mysqldump_quick
     */
    _mysqldump = App.spawn('mysqldump', [
        `-h${_db_host}`,
        `-u${_db_user}`,
        `-p${_db_pass}`,
        `${_db_name}`,
        '--column-statistics=0',
        '--single-transaction',
        '--quick'
    ]),

    _db_instance_id = App.config.db_instance_id,
    _db_replica_instance = false,
    _bucket = App.config.bucket,
    _main_prefix = _db_name,
    _retention_period = App.config.retention_period,
    _backup_dates_delete = [],
    _key, _mysql
    ;

const tasks = [

    // Step 1: Giving a Name for a new backup file of db by date and time...
    () => {

        console.log(
            'The process of making a backup of db and store it in the AWS S3 bucket is started.\n',
            `AWS RDS DB Instance: ${_db_instance_id}\n`,
            `MySQL DB Name: ${_db_name}\n`,
            `AWS S3 Bucket: ${_bucket}`
        );

        let datetime_now = App.lib.date_time.iso_timezone_datetime(Config.default_timezone);
        let date_today = datetime_now.substring(0, 10);

        _key = _main_prefix + `/${date_today}/${_db_name}-${datetime_now}AU.sql.gz`;

        App.lib.process.next();
    },

    // Step 2: Checking DB Instance is a Replica DB Instance or is NOT...
    () => {

        console.log(`\nChecking DB Instance [${_db_instance_id}] is a Replica DB Instance or is NOT...`);

        App.lib.aws_requests.rds.get_db_instances({DBInstanceIdentifier: _db_instance_id})
        .then(dbInstance => {
            if (dbInstance[0].ReadReplicaSourceDBInstanceIdentifier)
            {
                console.log('[!] It is a Replica!');

                _db_replica_instance = true;
            }
            else
                console.log('[!] It is NOT a Replica!');

            App.lib.process.next();
        });
    },

    // Step 3: Stopping the replication rule on the DB Instance...
    () => {

        if (!_db_replica_instance)
        {
            App.lib.process.next();
            return;
        }

        _mysql = new App.lib.MySQL(_mysql_connect_params);

        console.log('\nStopping the DB Instance replication...');

        _mysql.makeQuery('CALL mysql.rds_stop_replication').then(response => {
            console.log(response);
            console.log('[!] The replication is stopped.');

            App.lib.process.next();
        });
    },

    // Step 4: Making mysqldump db and backup to AWS S3 via stream...
    () => {

        console.log(
            '\nStarting the stream process, making mysqldump of db and store it in the AWS S3 bucket...\n',
            `MySQL DB Name: ${_db_name}\n`,
            `S3 Bucket: ${_bucket}`
        );

        const gzip = App.zlib.createGzip();

        _mysqldump.stdout
            .pipe(gzip)
            .pipe(App.lib.aws_s3.upload_from_stream(_key, _bucket, (response) => {
                    console.log(response);

                    App.lib.process.next();
                })
                .on('finish', () => console.log('<<< [!] Streaming is complete. >>>'))
                .on('error', (err) => App.lib.process.exit(err))
            );
    },

    // Step 5: Starting the replication rule on the DB Instance...
    () => {

        if (!_db_replica_instance)
        {
            App.lib.process.next();
            return;
        }

        console.log('\nStarting the DB Instance replication and disconnecting from MySQL...');

        _mysql.makeQuery('CALL mysql.rds_start_replication').then((response) => {
            console.log(response);
            console.log('[!] The replication is started.');

            _mysql.disconnect().then(() => App.lib.process.next());
        });
    },

    // Step 6: Starting the process of clearing old backups and getting all backups from S3 bucket...
    () => {

        if (!_retention_period)
        {
            console.log('[!] The process of clearing old backup dates is disable.');
            App.lib.process.time();

            App.lib.process.exit();
        }

        console.log(`\n[!] Clearing old backups of db [${_db_name}] that older than [${_retention_period}] days...`);
        console.log(`Getting all backup dates of db [${_db_name}] from AWS S3 Bucket...`);

        const params = {
            Bucket: _bucket,
            Prefix: _main_prefix
        };

        App.lib.aws_s3.get_list_objects(params).then(objects => App.lib.process.next(objects));
    },

    // Step 7: Parsing objects from S3 bucket and filtering old backup dates...
    (objects) => {

        let date_today = new Date(App.lib.date_time.iso_timezone_datetime(Config.default_timezone).substring(0, 10)),
            dateISO
            ;

        console.log(`Getting backup dates for clearing that are older than [${_retention_period}] days...`);

        objects.forEach(item => {
            dateISO = item.split('/')[1];
            const date = new Date(dateISO);
            const diffDays = App.lib.date_time.date_diff_in_days(date, date_today);

            if (!_backup_dates_delete.includes(dateISO) && diffDays > _retention_period)
                _backup_dates_delete.push(dateISO);
        });

        console.log({BackupDatesDelete: _backup_dates_delete});

        App.lib.process.next();
    },

    // Step 8: Clearing old backup dates in AWS S3 bucket...
    () => {
        if (_backup_dates_delete.length)
        {
            console.log('Clearing old backup dates in AWS S3 bucket...');

            App.lib.promise_queue(_backup_dates_delete, (dateISO) => new Promise(resolve_one => {

                const params = {
                    Bucket: _bucket,
                    Prefix: _main_prefix + '/' + dateISO
                };

                App.lib.aws_s3.delete_folder(params)
                .then(response => {
                    console.log(`[!] Old backup date files [${params.Prefix}] were cleared.`);
                    console.log(response);

                    resolve_one();
                });

            }))
            .then(() => App.lib.process.next());
        }
        else
        {
            console.log('[!] There is nothing so old and to clear.');

            App.lib.process.next();
        }
    },

    // Step 9: Showing the process time and exiting from the script...
    () => {
        let process_time = App.lib.process.time();

        console.log(`\nThe process time: ${process_time}`);
        console.log('Done!');

        App.lib.process.exit();
    }

];

// Catch for uncaught exceptions:
process.on("uncaughtException", (err) => App.lib.process.exit(err));

// Starting the process of creating a backup MySQL DB...
App.lib.process.start(tasks);
