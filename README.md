# AWS RDS Backup MySQL to AWS S3

## About

**AWS RDS Backup MySQL Database to AWS S3 Bucket**

**Script:** [`_rds_create_backup.js`](_rds_create_backup.js)  
**AWS SDK:** [` for JavaScript in Node.js`](https://aws.amazon.com/sdk-for-node-js/)  
**Language:** `JavaScript in Node.js`  

This script gives the automated process of creating a backup of MySQL DB on AWS RDS Instance from 
an existing DB Instance in AWS RDS and saving it in AWS S3 bucket.

**Author:** `Oleksii Pavliuk` _(pavliuk.aleksey@gmail.com)_  
**Date:** `2020-07-28`  

## Before using scripts:
- make sure you have read-only permissions to **MySQL on AWS RDS** and write-only to **AWS S3 Bucket**;
- [install AWS SDK and Dependencies](https://aws.amazon.com/developers/getting-started/nodejs/).

## Features
There are some scripts features which you need to know!

The script does:
- checks the AWS RDS DB Instance replication rule from Source DB Instance;
- if DB Instance is replica, script will turn off replication before making backup and, after backing turn on;
- uses stream for make a large db backup without saving on the host;
- searches folders in the bucket with backups that are old and clear it.  

`[!]` The script turns off replication rule, if DB Instance is replica and has a source DB Instance (Master).
      After the backup process it will turn on the replication from Master DB.

