/**
 * Promise Queue Module.
 */

/**
 * Exports Promise Queue Module.
 *
 * Promise for synchronizing execute
 * the function with every item of the array.
 *
 * @param {any[]} _array
 *      An array of anything.
 * @param {Function} _Promise
 *      A Promise function that executes
 *      with every item of array.
 *
 */
module.exports = (_array, _Promise) => new Promise(resolve => {

    _manager(0, _array, _Promise, () => {
        resolve();
    });

});

// Private Functions:

/**
 * Promise Recursive Manager.
 *
 * Recursive one by one goes put an array index
 * to Promise() that we gave like a parameter _Promise.
 * When the manager meets the end of the array,
 * it calls the callback and exit from the function.
 *
 * @param {integer} key
 *      A index of array.
 * @param {any[]} _array
 *      An array of anything.
 * @param {Function} _Promise
 *      A Promise function that executes
 *      with every item of array.
 * @param {Function} callback
 *      The exit from recursive.
 *
 */
let _manager = (key, _array, _Promise, callback) => {
    if (key >= _array.length)
        return callback();

    _Promise(_array[key]).then(() => {
        _manager(key+1, _array, _Promise, callback);
    });
};
